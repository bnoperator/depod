#Depod
Depod is a human phosphotase database and can be found at: http://www.koehn.embl.de/depod/br_p.php

This code webscrapes the online database and creates text files.

One of the text files is an enrichment file to be used with BioNavigator.

##Install
Checkout the source:
```
git clone https://fnaji@bitbucket.org/bnoperator/depod.git
```

##Getting started
To generate the output files run all code in "workspace.R"

This will produce a set of files.

The output enrichment file is called "ptp_final.txt"

##Comments
Code which splits a column in a specified file is a hack, I admit. Still thinking how to elegantly split columns in a file.
